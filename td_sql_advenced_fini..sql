-- créer les tables visit, place et person,
--faire un read me avec les détails des dossiers , et dans chaque dossiers
-- ce qu'il y a , et ce que j'ai fait en gros

drop table if exists person;

CREATE table person (
person_id INT not null,
person_name VARCHAR (50) not null,
health_status VARCHAR (50) not null,
confirmed_status_date TIMESTAMP not null,
constraint pk_person primary key  (person_id));


select * from person;

drop table if exists place;

CREATE table place (
place_id INT not null,
place_name VARCHAR (50) not null,
place_type VARCHAR (50) not null,
constraint pk_place primary key  (place_id));

drop table if exists visit;

CREATE table visit (
visit_id INT not null,
person_id INT not null,
place_id INT not null,
start_datetime TIMESTAMP not null,
end_datetime TIMESTAMP not null,
constraint pk_visit primary key (visit_id),
constraint fk_visit_person foreign key (person_id) references person (person_id),
constraint fk_visit_place foreign key (place_id) references place (place_id));



select * from person;
select * from place; 
select * from visit;
---------------------------------------------------------------------------------------
--Noms + statuts santé des personnes que Landyn Greer a croisé (même lieu, même moment)
---------------------------------------------------------------------------------------


select * from person where person_name = 'Landyn Greer';
-- Landyn Greer, person_id = 1

select * from visit  where person_id = '1';
-- on récupère les informations associés à Landyn Greer, 2 visit_id, 3 106 avec place_id 33, et 3 937 avec 88. 
--- les dates séparements 

select * from visit where start_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000';

select * from visit where end_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000';
-----pour place_id 33

select * from visit where start_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000';

select * from visit where end_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000';

-- j'assemble les dates sans que landyn greer ne soit compris 

select * from visit 
where place_id = 33 
	and ( start_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000'
		or end_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000')
and person_id !=  1
or place_id = 88
	and (start_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000'
		or end_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000')
and person_id !=  1;


---------------final mais avec Landyn Greer conflit person_id------------------------------------------------------- 

select person_name, health_status from person p inner join visit v on p.person_id  = v. person_id 
where person_name != 'Landyn Greer' and ( place_id = 33 
	and ( start_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000'
		or end_datetime between '2020-10-07 20:54:00.000' and '2020-10-08 03:44:00.000')
or place_id = 88
	and (start_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000'
		or end_datetime between '2020-09-26 18:28:00.000' and  '2020-09-26 21:40:00.000')); 
	

--------------------------------------------------------------------------------------------------------------------
--Nombre de malades croisés dans un bar (même moment)
-----------------------------------------------------------

-- Associe les 3 tableaux avec inner join
	
select * from place a inner join visit v 
	on a.place_id = v.visit_id
inner join person p on p.person_id = v.visit_id 
where health_status = 'Sick'
and place_type = 'Bar' ;


-- ajout de count pour avoir le nombre de malade dans les bars 

select place_type , count(health_status) as nb_sick_person_bar
from place a inner join visit v on a.place_id = v.place_id 
inner join person p on p.person_id = v.visit_id 
where health_status = 'Sick'
and place_type = 'Bar'
group by place_type;


------------pour avoir le nombre de malade croisé dans un même bar 

select place_name, count(health_status) as nb_sick_person_bar from place a inner join visit v 
	on a.place_id  = v.place_id
inner join person p on p.person_id = v.person_id 
where health_status = 'Sick'
and place_type = 'Bar' 
and confirmed_status_date > start_datetime
group by place_name;

-------------------------------------------------------------
--Noms des personnes que Taylor Luna a croisé
-------------------------------------------------------------

select * from person where person_name = 'Taylor Luna';
select * from visit where person_id = '4';



--- après inner join avec person et select person_name 

select person_name  from person p LEFT OUTER join visit v on p.person_id = v.person_id where ...; 


-- ca pour les 10 lieux mais doit avoir une autre technique.

select person_name from person p inner join visit v on p.person_id  = v. person_id 
where  person_name != 'Taylor Luna' 
	and (place_id = 72 and ( start_datetime between '2020-09-23 05:51:00.000' and '2020-09-23 11:05:00.000'
		or end_datetime between '2020-09-23 05:51:00.000' and '2020-09-23 11:05:00.000')
or place_id = 8
	and (start_datetime between '2020-09-18 23:37:00.000' and  '2020-09-19 05:52:00.000'
		or end_datetime between '2020-09-18 23:37:00.000' and  '2020-09-19 05:52:00.0000') 
or place_id = 84
	and (start_datetime between '2020-09-13 20:47:00.000' and  '2020-09-14 07:22:00.000'
		or end_datetime between '2020-09-13 20:47:00.000' and  '2020-09-14 07:22:00.000')
or place_id = 26
	and (start_datetime between '2020-09-23 11:27:00.000' and  '2020-09-23 20:28:00.000'
		or end_datetime between '2020-09-23 11:27:00.000' and  '2020-09-23 20:28:00.000')
or place_id = 5
	and (start_datetime between '2020-10-02 04:07:00.000' and  '2020-10-02 14:13:00.000'
		or end_datetime between '2020-10-02 04:07:00.000' and  '2020-10-02 14:13:00.000')
or place_id = 84
	and (start_datetime between '2020-10-09 19:18:00.000' and  '2020-10-10 04:52:00.000'
		or end_datetime between '2020-10-09 19:18:00.000' and  '2020-10-10 04:52:00.000')
or place_id = 29
	and (start_datetime between '2020-10-09 15:03:00.000' and  '2020-10-10 01:51:00.000'
		or end_datetime between '2020-10-09 15:03:00.000' and  '2020-10-10 01:51:00.000')
or place_id = 90
	and (start_datetime between '2020-10-03 22:19:00.000' and  '2020-10-04 07:19:00.000'
		or end_datetime between '2020-10-03 22:19:00.000' and  '2020-10-04 07:19:00.000')
or place_id = 4
	and (start_datetime between '2020-09-14 15:40:00.000' and  '2020-09-15 06:23:00.000'
		or end_datetime between '2020-09-14 15:40:00.000' and  '2020-09-15 06:23:00.000')
or place_id = 52
	and (start_datetime between '2020-09-15 06:23:00.000' and  '2020-10-09 23:06:00.000'
		or end_datetime between '2020-09-15 06:23:00.000' and  '2020-10-09 23:06:00.000'));
	


------------------------------------------------------------------------------------------------
--Nombre de malades par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
------------------------------------------------------------------------------------------------

-- la durée moyenne des visites avec avg --> duree_visit, avec le nombre de malade , ajout 
-- de la condition après que le status est était déclaré, donc c'est les malades déclarés
-- dans un endroit avec place_name 


select avg (end_datetime - start_datetime) as duree_visit, place_name, count(p.person_id) as nb_person_sick 
from place a inner join visit v 
	on a.place_id = v.place_id
inner join person p on p.person_id = v.person_id 
where health_status = 'Sick' 
and confirmed_status_date > start_datetime
group by place_name;


-----------------------------------------------------------------------------------------------------------
--Nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
-----------------------------------------------------------------------------------------------------------

-- même chose qu'avant mais avec les personnes saines, sans condition de status déclarer, ils sont censés être 
-- sains même avant. 


select avg (end_datetime - start_datetime) as duree_visit, place_name, count(*) as nb_person_healthy 
from place a inner join visit v 
	on a.place_id = v.place_id
inner join person p on p.person_id = v.person_id 
where health_status = 'Healthy' 
group by place_name;



	





	